General downloads repo.

# List of downloads

## V

* [vimrc-2016-06-08.bak.tar.gz](https://gitlab.com/bradenbest/downloads/blob/master/vimrc-2016-06-08.bak.tar.gz) -- My vimrc as of 2016-06-07
* [x.bak.2016-05-27.tar.gz](https://gitlab.com/bradenbest/downloads/blob/master/x.bak.2016-05-27.tar.gz) -- A backup of my scripts which happens to also include a bunch of copyrighted wallpapers. So yeah, do me a favor and don't go around distributing this without omitting the wallpapers.
* [minutes.png](https://gitlab.com/bradenbest/downloads/blob/master/minutes.png) -- An 11KB image that manages to crash both sxiv and feh.
